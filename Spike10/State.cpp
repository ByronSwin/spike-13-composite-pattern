#include "State.h"


using namespace std;

/*
	The abstract class for states. All states must be able to display output, handle commands and get user input.

	@param ids for this state
*/
State::State(vector<string> ids)
:IObject(ids)
{

}

State::~State()
{
}