#pragma once

#include "Location.h"
#include "IObject.h"

class Edge : public IObject
{
public:
	Edge(std::vector<std::string> ids,Location* out);
	Location* getOut();
	~Edge();

private:
	Location* _out;
};

