#include "FailedCommand.h"

using namespace std;

/*
	The failed commmand, inherits from command. This command is used as a catch all, and has no variables or getters.

	@param ids the ids used for this command
*/
FailedCommand::FailedCommand(vector<string> ids)
: Command(ids)
{
}


FailedCommand::~FailedCommand()
{
}
