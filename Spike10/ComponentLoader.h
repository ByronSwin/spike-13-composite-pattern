#pragma once

#include "Component.h"
#include "CarryComponent.h"
#include "WeaponComponent.h"
#include "InventoryComponent.h"
#include "LockComponent.h"
#include "Entity.h"


#include <fstream>
#include <iostream>
#include <string>

class ComponentLoader
{
public:
	ComponentLoader();

	Entity* LoadEntity(std::ifstream& aIftream);
	~ComponentLoader();
private:
	std::vector<std::string> SplitString(std::string input);
};

