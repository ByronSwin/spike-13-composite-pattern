#pragma once

#include <vector>
#include <algorithm>
#include <string>
#include <map>

#include "Command.h"

#include "QuitCommand.h"
#include "FailedCommand.h"
#include "StateCommand.h"
#include "StartCommand.h"
#include "LookCommand.h"
#include "GoCommand.h"
#include "TakeCommand.h"

class CommandProcessor
{
public:
	CommandProcessor();
	~CommandProcessor();

	Command* Process(std::string input);

private:
	std::map<std::string, Command*> _commands;
	std::vector<std::string> SplitString(std::string input);
};

