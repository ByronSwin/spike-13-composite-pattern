#pragma once


#include "State.h"
#include "MainMenuState.h"
#include "InformationState.h"
#include "SelectAdventureState.h"
#include "GameplayState.h"
#include "HighscoreState.h"
#include "Command.h"

#include "StateCommand.h"
#include "StartCommand.h"

#include "GameData.h"


#include <map>

class ContextController 
{
public:
	ContextController();
	~ContextController();

	void SetState (std::string newState);
	std::string GetInput();
	void Handle(Command* cmd);
	void Display();

private:
	std::map<std::string, State*> _states;
	State* _currentState;
	State* _previousState;
	GameData* _theGame;
};

