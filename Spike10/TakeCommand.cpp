#include "TakeCommand.h"

using namespace std;

/*
	Take command, inherits from command. Take command indicates the user wants to add an item to their inventory. Has variables for an error message,
	take identifier and from identifier.

	@param ids identifiers for the take command.
*/
TakeCommand::TakeCommand(vector<string> ids)
: Command(ids)
{
	_errMsg = "";
	_takeId = "";
	_fromId = "";
}

void TakeCommand::Update(vector<string> input)
{
	_errMsg = "";
	_takeId = "";
	_fromId = "";

	if (input.size() == 3)
	{
		_errMsg = "NOERROR";
		_takeId = input.at(2);
		_fromId = "ROOM";
		
	}
	else if (input.size() == 5)
	{
		if (input.at(3).compare("FROM") == 0)
		{
			_errMsg = "NOERROR";
			_takeId = input.at(2);
			_fromId = input.at(4);
		}
		else
		{
			_errMsg = "Error in take command structure";
		}
	}
	else if (input.size() == 6)
	{
		if (input.at(3).compare("FROM") == 0 &&
			input.at(4).compare("THE") == 0)
		{
			_errMsg = "NOERROR";
			_takeId = input.at(2);
			_fromId = input.at(5);
		}
		else
		{
			_errMsg = "Error in take command structure";
		}
	}
	else if (input.size() == 2)
	{
		_errMsg = "Take from what?";
	}
	else
	{
		_errMsg = "Error in take command structure";
	}
}

string TakeCommand::GetErrMsg()
{
	return _errMsg;
}

string TakeCommand::GetFromId()
{
	return _fromId;
}

string TakeCommand::GetTakeId()
{
	return _takeId;
}

TakeCommand::~TakeCommand()
{
}
