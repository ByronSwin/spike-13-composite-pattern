#include "CommandProcessor.h"

using namespace std;

/**
	The command processer that handles the raw input and turns it into a useful command. It contains a map of
	all current commands.

	The constructor fills the _commands map with pointers to the applicable commands.

*/
CommandProcessor::CommandProcessor()
{
	_commands["FAILED"] = new FailedCommand(vector<string>() = { "FAILED" });

	_commands["ADVENTURESELECT"] = new StartCommand(vector<string>() = { "ADVENTURESELECT" });
	StateCommand* stCmd = new StateCommand(vector<string>() = { "HOF", "SELECT", "START", "ABOUT", "MAINMENU", "PREVIOUS", "HELP" });
	_commands["HOF"] = stCmd;
	_commands["HELP"] = stCmd;
	_commands["SELECT"] = stCmd;
	_commands["START"] = stCmd;
	_commands["ABOUT"] = stCmd;
	_commands["MAINMENU"] = stCmd;
	_commands["PREVIOUS"] = stCmd;
	_commands["INFORMATION"] = stCmd;

	GoCommand* goCmd = new GoCommand(vector<string>() = { "GO", "TRAVEL", "MOVE"});
	_commands["GO"] = goCmd;
	_commands["TRAVEL"] = goCmd;
	_commands["MOVE"] = goCmd;

	TakeCommand* takeCmd = new TakeCommand(vector<string>() = { "TAKE", "GRAB" });
	_commands["TAKE"] = takeCmd;
	_commands["GRAB"] = takeCmd;


	_commands["LOOK"] = new LookCommand(vector<string>() = { "LOOK"});

	_commands["QUIT"] = new QuitCommand(vector<string>() = { "QUIT", "Q" });
	
}

/*
	Clears the _commands map
*/
CommandProcessor::~CommandProcessor()
{
	_commands.clear();
}

/*
	The main method for handeling raw input. The first step is to turn it all into upper case,
	the second is to split the sring by the delimeter " ". Lastly, it uses the vector to find the relevant command.

	The order of the checking is important - always checking if the user has requested to quit or for help before checking where the input has come from.

	Once the relevant command has been found, pass in the input into that command and then return it.

	@param input the raw input
*/
Command* CommandProcessor::Process(string input)
{
	transform(input.begin(), input.end(), input.begin(), ::toupper);
	vector<string> splitput = SplitString(input);

	if (splitput.size() > 1)
	{
		if (splitput.at(1).compare("QUIT") == 0)
		{
			return _commands["QUIT"];
		}
		else if (splitput.at(1).compare("HELP") == 0)
		{
			_commands["HELP"]->Update(splitput);
			return _commands["HELP"];
		}
		else if (splitput.at(0).compare("GAMEPLAY") == 0)
		{
			if (splitput.at(1).compare("LOOK") == 0)
			{
				_commands["LOOK"]->Update(splitput);
				return _commands["LOOK"];
			}
			else if (splitput.at(1).compare("GO") == 0
				|| splitput.at(1).compare("TRAVEL") == 0
				|| splitput.at(1).compare("MOVE") == 0)
			{
				_commands["GO"]->Update(splitput);
				return _commands["GO"];
			}
			else if (splitput.at(1).compare("TAKE") == 0
				|| splitput.at(1).compare("GRAB") == 0)
			{
				_commands["TAKE"]->Update(splitput);
				return _commands["TAKE"];
			}


		}
		else
		{
			_commands[splitput.at(0)]->Update(splitput);
			return _commands[splitput.at(0)];
		}
	}


	return _commands["FAILED"]; // If no suitable command is found or it fails in size, return a failed command.
	
}

/*
	A method used for taking in a string and returning it as a vector of strings, split by the delimeter " ".

	@param input the string to split up
*/
vector<string> CommandProcessor::SplitString(string input)
{
	string next;
	vector<string> result;

	for (string::const_iterator it = input.begin(); it != input.end(); it++)
	{
		if (*it == ' ')
		{
			if (!next.empty())
			{
				result.push_back(next);
				next.clear();
			}
		}
		else
		{
			next += *it;
		}
	}
	if (!next.empty())
	{
		result.push_back(next);
	}

	return result;
}