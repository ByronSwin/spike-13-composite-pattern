#pragma once
#include <vector>
#include <string>
#include <map>
#include "Message.h"

class Blackboard
{
public:
	
	~Blackboard();

	static void Add(Message* newMsg);

	static Message* Access(std::string accesor);
	
};

