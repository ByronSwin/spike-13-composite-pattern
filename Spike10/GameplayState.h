#pragma once

#include "State.h"
#include "InventoryComponent.h"


#include "GameData.h"

class GameplayState : public State
{
public:
	GameplayState(std::vector<std::string> ids, GameData* theGame);
	~GameplayState();
	void Handle(Command* cmd);
	void Display();


	std::string GetInput();

private:
	void RefreshDefault();

	GameData* _theGame;
	std::string _displayString;
	std::string _defaultString;

};

