#include "IObject.h"

using namespace std;

/*
	IObject class. Handles using identifiers for objects using strings. 

	@param ids for this IObject
*/
IObject::IObject(vector<string> ids)
{
	_ids = ids;
}

/**
	Virtual method for recieving messages
	
*/
void IObject::Recieve(Message* toRec)
{

}

/*
	Checks if this IObject is identifiable by the parameter

	@param id string identifier to check
	@returns bool true if this IObject matches the parameter
*/
bool IObject::AreYou(string id)
{
	
	for (auto &currId : _ids)
	{
		if (currId.compare(id) == 0)
		{
			return true;
		}
	}

	return false;
}

/*
	@returns modified identifer for this object.
*/
string IObject::FirstId()
{
	return "(" + _ids[0] + ")";
}


IObject::~IObject()
{
}
