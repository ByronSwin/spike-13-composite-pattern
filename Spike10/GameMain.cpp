#include "GameMain.h"

using namespace std;

/*
	Main game class. Handles the main loop (is the glue)
*/
GameMain::GameMain()
{
	_Controller = new ContextController();
	_CmdProcessor = new CommandProcessor();
}

/*
	Tells the gamemain to start and run the game. Continues to run until the quit command is seen
*/
void GameMain::run()
{
	string currInput = "NOINPUT";
	Command* currCommand = _CmdProcessor->Process(currInput);
	
	while (true)
	{
		_Controller->Display(); // Displays the current state
				
		currInput = _Controller->GetInput(); //Gets the input from the user

		currCommand = _CmdProcessor->Process(currInput); //Turns the input into a command

		_Controller->Handle(currCommand); //Tells the current state (or context controller) to handle the command
			

		if (dynamic_cast<QuitCommand*>(currCommand) != nullptr) //If quit command, close game
		{
			cout << "Thanks for playing" << endl;
			break;
		}
		

	}
}


GameMain::~GameMain()
{
}
